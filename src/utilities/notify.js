import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const showSuccess = (msg) => {
    toast.success(msg);
    
}

const showInfo = (msg) => {
    toast.info(msg);
}

const showWarning = (msg) => {
    toast.warn(msg);

}

const showError = (errMsg) => {
    toast.error(errMsg);
}

const handleError = (error) => {
    debugger;
    //collect error from the entire application
    //check for the error
    //parse the error message
    //show the appropriate error message
    // console.log(
    // showError(JSON.stringify(error))

    const err = error && error.response && error.response.data;
    if(err)
    {
        if (typeof (err.msg) == 'string') {
        }
    }
}


export default {
    showSuccess,
    showInfo,
    showWarning,
    handleError,
    showError
}