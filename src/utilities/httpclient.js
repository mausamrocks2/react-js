// hami axios register.component.js ma axios call gardainam, yo file ra tyo file observable bata garaula, yei kaam httpcient.js bata garney
// uta axios.post ma full url diyeko thiyo,tara yaha euta connectivity ko lagi base url diyera communication garney
// subURL chai tala diney, baseUrl KO sanga connect garney jastai localhost:3030..
// each time u edit on .env start npm start again...
import axios from 'axios';

//base instance
//malai jatajata http call garnu parcha, sidhai APICLient lai call garchu ani tespachi get,put,post k garne ho call gardinchu url pathaidiyera,data k ho pathaidichu 
const baseURL = process.env.REACT_APP_BASE_URL
console.log('base url>>', baseURL);
const http = axios.create({
  baseURL: baseURL,
  responseType: 'json',
})



const requestHeaders = {
  'Content-Type': 'application/json'
}

const requestHeadersWithToken = {
  'Content-Type': 'application/json',
  'Authorization': localStorage.getItem('token')
    //FOR TOKEN
  
}

/**
 * http get request
 * @param {string} url 
 * @param {object} headers
 */

//{} uta bata object audo raicha, tyo object bhitrak ako propertylai destruct gardo raicha 
//jastai headers audo raicha ayena bhane tesko satta empty object liu
//get request ma kailai pani body audaina
//kasailey get request call garda 2nd arg napathaye pani empty obj audo raicha

// observables TODO
//calling http, and sabai kura pathaidinu paryo
//yelle malai promise dincha axios based httpclient ho, yelle k promise dincha tyoo kura maile return gardinchu
//with .then and .catch
// then ley entire data matra haina response matra pauchu....
//error matra heryo bhane http error matra dekhcham not server error so we did this
function get(url, { headers = requestHeaders, params = {}, responseType = 'json' } = {}, secured= false)
{ 
return http({
  method: "GET",
  url,
  headers: secured ? requestHeadersWithToken: requestHeaders,
  params,
  responseType
})
// .then(response => response)
.then(data => data.data)
.catch(err => err.response);
}

function put(url, { headers = requestHeaders, body = {}, params = {}, responseType = 'json' } = {}, secured = false)
{ 
// observables TODO
return http({
  method: "PUT",
  url,
  headers: secured ? requestHeadersWithToken: requestHeaders,
  data: body,
  params,
  responseType
})
.then(data => data)
}

function post(url, { headers = requestHeaders, body = {}, params = {}, responseType = 'json' } = {},secured = false)
{ 
// observables TODo
return http({
  method: "POST",
  url,  
  headers: secured ? requestHeadersWithToken: requestHeaders,
  data: body,
  params,
  responseType
})
.then(data => data.data);
// .catch((err) => {
//   console.log('err>>>',err.response);
//   return err.response
// })
}

function remove(url, { headers = requestHeaders, params = {}, responseType = 'json' } = {}, secured = false)
{ 
// observables TODO
return http({
  method: "DELETE",
  url,
  headers: secured ? requestHeadersWithToken: requestHeaders,
  params,
  responseType
})
.then(data => data.response)
.catch(err => err.response);
}

function upload(method, url, data, files)
{

  const promise = new Promise((resolve, reject) => {

  
  debugger;
  const xhr = new XMLHttpRequest();
  const formData = new FormData();
  if (files.length)
  {
    formData.append('img', files[0], files[0].name)
  }

  for (let key in data)
  {
    formData.append(key, data[key]);
  }

  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4)
    { 
      if (xhr.status === 200)
      { 
        resolve(xhr.response)

      }
      else{
        reject(xhr.response);
      }
    }
  }
  xhr.open(method, url, true)
  xhr.send(formData);
})
return promise;
}


export default{
  get,
  post,
  put,
  delete: remove,
  upload,
}



// import axios from "axios";

// const http = axios.create({
//   baseURL: process.env.REACT_APP_BASE_URL,
//   responseType: "json",
// });

// const usBodyHeader = {
//   "Content-type": "application/json",
//   // 'Content-type': 'application/x-www-form-urlencoded'
// };



// function get(url, { params = {}, responseType = "json" } = {}) {
//   return http({
//     method: "GET",
//     url,
//     headers: usBodyHeader,
//     params,
//     responseType,
//   });
// }

// function post(url, { body = {}, responseType = "json" } = {}) {
//   return http({
//     method: "POST",
//     url,
//     headers: usBodyHeader,
//     data: body,
//     responseType,
//   });
// }

// function put(url, { body = {}, responseType = "json" } = {}) {
//   return http({
//     method: "PUT",
//     url,
//     headers: usBodyHeader,
//     data: body,
//     responseType,
//   });
// }

// function remove(
//   url,
//   { params = {}, responseType = "json" } = {},
// ) {
//   return http({
//     method: "DELETE",
//     url,
//     headers: usBodyHeader,
//     params,
//     responseType,
//   });
// }

// function upload(url, method, files) {
//   return new Promise((resolve, reject) => {
//     const xhr = new XMLHttpRequest();
//     const formData = new FormData();
//     if (files.length) {
//       formData.append("img", files[0], files[0].name);
//     }
//     xhr.onreadystatechange = () => {
//       if (xhr.readyState === 4) {
//         resolve(xhr.response);
//       }
//     };
//     xhr.open(method, url, true);
//     xhr.send(formData);
//   });
// }

// export default {
//   get,
//   post,
//   put,
//   delete: remove,
//   upload,
// }