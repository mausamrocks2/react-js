//assume this file is routing configuration
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Register } from './components/register/register.component';
import { Login } from './components/login/login.component';
import Navbar from './components/header/navbar.component';
import Sidebar from './components/sidebar/sidebar.component';
import { Dashboard } from './components/dashboard/dashboard.component';
import { AddProduct } from './components/product/add-product.component';
import { ViewProduct } from './components/product/view-product.component';
import { EditProduct } from './components/product/edit-product.component';
import { SearchProduct } from './components/product/search-product.component';
import { ForgotPassword } from './components/forgot-password/forgot-password.component';
import { ResetPassword } from './components/reset-password/reset-password.component';
import { ChatComponent } from './components/chat/chat.component';





class NotFound extends Component
{   
    render()
    {
    return(
    <div>
        <h2>i am not found</h2>
        <p>page error 404 not found</p>
    </div>
    );
}
}


// WAY OF DECLARING THE FUNCTIONAL COMPONENT THROUGH REST METHOD..
//protectedRoute call garda jati pani props haru pathauda attr and values haru component mausam ma aru
//baki value haru {...abc ma }  state ho yo chahi
//reference is ==> react dom render and react admin template
//<Mausam/>  component ma props haru like login,register...... 
//path chai {abc.path} and component chai <Mausam/>  
//<>  </>  fragment empty divs, if u dont want to use unwanted many divs
//protected route la pani route nai return garney ho ani exact={true} pani ...abc mai jancha
const ProtectedRoute = ({ component: Component, ...data }) => {
    return (
        
        //pachi render ma props auna sakcha componentma
        //props ma j pani auna sakcha
        <Route path={data.path} render={(props) => {
            return ( 
                localStorage.getItem('token')
        ? (
        <>
        <div className="navmenu">
            <Navbar/>
        </div>

        <div className="sidemenu">
            <Sidebar/>       
        </div>
        <Component {...props}/>
        </>
            
    )
    : (
        <Redirect to="/" /> //TODO redirect will take the additional props
    )
        )
        }} />
    )
}
//sabai protectedroute haruma token haru check garne bhayo,localstorage ma token cha bhane-
//components aune bhayo dashboard or whatever , chaina bhane redirect to login 


const appRouting = () => {
    return (
        <Router>
        <div>
        <div className="content">
        <Switch>
            <Route exact path="/" component={Login}></Route>
            <Route path="/register" component={Register}></Route>
            <Route path="/forgot-password" component={ForgotPassword}></Route>
            <Route path="/reset-password/:token" component={ResetPassword}></Route>

            {/* <Route path="/about" component={About}></Route> */}
            
            <ProtectedRoute path="/dashboard" component={Dashboard} />
            <ProtectedRoute path="/product/add" component={AddProduct} />
            <ProtectedRoute path="/product/edit/:id" component={EditProduct} />
            <ProtectedRoute path="/product/view" component={ViewProduct} />
            <ProtectedRoute path="/product/search" component={SearchProduct} />
            <ProtectedRoute path="/chat" component={ChatComponent} />


            {/* sidebar ma jancha link to cha  */}
            <Route path="" component={NotFound}></Route>
            </Switch>
        </div>  
        </div>
        </Router>
            
            
    )
}

export default appRouting;
    