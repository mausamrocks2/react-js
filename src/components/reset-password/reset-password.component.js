import { React, Component }  from 'react';
import http from './../../utilities/httpclient';
import notify from './../../utilities/notify';

const defaultForm = {
    password: '',
    confirmPassword:''
}



export class ResetPassword extends Component 
{
    id;
    constructor()
    {
        super();
            this.state = {
                data: {
                        ...defaultForm
                },
                error: {    
                        ...defaultForm
                },
                validFrom: false,
                isSubmitting: false,
            }
        
        
    } 

    componentDidMount()
    {
        this.id = this.props.match.params['token'];       
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => this.validateErrors(name))

    }

    validateErrors(fieldName) 
    {
        let err;
        switch(fieldName)
        {
            case 'password':
                err = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 2
                    ? ''
                    : "Weak Password"
                    : 'Password is required';
                break;
            case 'ConfirmPassword': 
                err = this.state.data[fieldName]
                    ? this.state.data[fieldName] === this.state.data.password
                    ? ''
                    : "Confirm Password did not match"
                    : 'Confirm Password is required';
                    break;
            default: 
                break;  
        }
        this.setState((pre) => ({
            error: {
                ...pre.error,
                [fieldName]: err 
            }
        }))
    }



    handleSubmit = (e) => {
        e.preventDefault();      //yaha aba data aucha, hamiley aba http call garnu paryo
        
        http.post(`/auth/reset-password/${this.id}`, {body: this.state.data })
        .then((data) => {
            notify.showSuccess('Password reset successful please login');
            this.props.history.push('/');
        })
        .catch((err) => {
            notify.handleError(err);

        })
    }



    render()
    {
        return (
            <>
            <h2>Reset Password</h2>
            <p>Choose your password wisely</p>
            <form className = "form-group" onSubmit= {this.handleSubmit}>
                <label>Password</label>
                <input className = "form-control" name= "password" type= "password" placeholder="Password " onChange = {this.handleChange}></input>
                <p className="danger">{ this.state.error.password }</p>
                
                <label>Confirm Password</label>
                <input className = "form-control" name= "confirmPassword" type= "password" placeholder="confirmPassword" onChange = {this.handleChange}></input>
                <p className="danger">{ this.state.error.confirmPassword }</p>
                <br />
                <button type = "submit" className = "btn btn-primary">Submitting...</button>
            </form>
            </>
        )
    }
}