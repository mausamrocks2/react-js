 //component is basic building block or react 
 //functional component
 //class based component

 //types of component
 //stateful component -- class based component

 //stateless component-- functional component
 //-- can be made stateful using react hooks


 //props --immutable (when components r called, attribute gets passed)
 //states -- mutable (within a component)

 //life cycle of component
 
 //parent-child component

 //higher order components

 import React from 'react'; 
import PropTypes from 'prop-types';

function Welcome(props)
 {
     //in function based components parameters of functions are props
     console.log('properties >>',props);
     return (
            <div>
            <h2> Welcome {props.name}</h2>
            <p> You the fucking smartest social animal of this entire universe</p>
            </div>

        
     );
 }
Welcome.defaultProps = {
    name: PropTypes.string,
    number: PropTypes.number.isRequired,
}

 export default Welcome;




