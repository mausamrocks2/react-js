
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './register.component.css';
import APIClient from './../../utilities/httpclient'; 
//APIClientma aba mero object aucha
import notify from '../../utilities/notify';

   




export class Register extends Component
{
constructor()
{
    super();
    this.state = {
        username: null,
        password: null,
        name: null,
        email: null,
        emailError: null,
        usernameError: null,
        passwordErr: null

    };
    this.handleChange = this.handleChange.bind(this);
}

handleChange(e)
{
    console.log('event name>>', e.target.name);
    console.log('event value>>', e.target.value);
    this.setState({
        [e.target.name]: e.target.value
    })
}
handleSubmit(e)
{
    // console.log(this.state);
    e.preventDefault();
    // console.log('here at submit>>');
    let emailError ;
    if(!this.state.email)
    {
        emailError = 'Email is required field';

    }
    else{
        emailError = this.state.email.includes('@')
        ? null
        : 'Invalid email format';
    }

    // if(emailError || usernameError || passwordError)
    if(emailError)

    {
        this.setState({

            emailError
            
        })
    }
    else
    {
        //Backend call now
        //API ho tei bhayera url chuttai tarikaley diney, aru baseURL ma cha, ani arko ma data body ma pathako cha
        //maile data headers pathako chaina uuu afailey lido raicha mailey pathauney bhaneko body
        // aba yo method ley malai promise return gardincha.... 
        APIClient.post('/auth/register', { body: this.state })
        .then(() => {
            console.log("registration successful")
            notify.showSuccess('registration successful');
            //props haru ma routing use garepachi routing related confog haru aucha like history
            this.props.history.push('/');   //redirect to login

        })

        .catch((err) => {
        notify.handleError(err);    //jata jata error aucha handleError patahidincham,rather than doing again and again console.log    
        })
    }


    

    
    //I SHOULD GET FORM DATA HERE
    //IF FORM DATA IS READY CALL BACKEND WITH THOSE DATA
    //NAVIGATION

    //ROUTING ===> NAVIGATION
    //AXIOS // PROMISE
    //RXJS //OBSERVABLES
}

render()
{
    let form = (
        <div>
            <h2>Register</h2>
            <p>Please provide your details to Register</p>
            <form className="form-group" onSubmit={this.handleSubmit.bind(this)} noValidate>
                <label htmlFor="name">Name</label>
                <input name="name" className="form-control" id="name" onChange={this.handleChange} type="text" placeholder="Name"></input>
                <label htmlFor="email">Email</label>
                <input name="email" className="form-control" id="email" onChange={this.handleChange} type="email" placeholder="Email" required></input>
                <p className="danger">{this.state.emailError}</p>
                <label htmlFor="username">username</label>
                <input name="username" className="form-control" id="username" onChange={this.handleChange} type="text" placeholder="UserName" required></input>
                <label htmlFor="password">password</label>
                <input name="password" className="form-control" id="password" onChange={this.handleChange} type="password" placeholder="Password" required></input>
                <br></br>
                {/* <button className="btn btn-primary" onClick={this.handleSubmit.bind(this)}>SUBMIT</button> */}
                <button className="btn btn-primary">SUBMIT</button>

            </form>
            <p>Already registered?</p>
            <p>Login <NavLink to="/">here</NavLink></p>
        </div>
        
    )
    return form; 
}
}


