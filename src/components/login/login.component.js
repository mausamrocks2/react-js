import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
import {NavLink} from 'react-router-dom';
import notify from './../../utilities/notify';
import http from './../../utilities/httpclient';



export class Login extends Component
{
    constructor(props){
        super(props);
        this.state = {
                username:'NewYork',
                password:'testing',
            
        }
    }
    
    handleSubmit(e)
    {
        e.preventDefault();
        http.post('/auth/login', {body: this.state})
        .then((data) => {
            notify.showInfo('Login successful');  //for developers
            notify.showInfo(`Welcome $(data.user.username`);   //template literals
            console.log('data>>>',data);

            //webstorage
            localStorage.setItem('user',JSON.stringify(data.user));
            localStorage.setItem('token',data.token);
            this.props.history.push('/dashboard');   //redirect to dashboard, dashboard ma jada matrai protectedroute check garney ho


        })
        .catch((err) => {
            notify.handleError(err);
        })
    }

    handleChange = (e) => {
       
        this.setState({[e.target.name]: e.target.value })

    }

     
    
    render()
    {
        
        return(
            <div>

            <h2>Login</h2>
            <h4>Welcome to our  Web Store</h4>
            <p>Please provide your details to Login</p>
            <form className="form-group" onSubmit={this.handleSubmit.bind(this)} noValidate>
                
                <label htmlFor="username">Username</label>
                <input name="username" className="form-control" id="name" onChange={this.handleChange} type="text" placeholder="UserName"></input>
                <label htmlFor="password">Password</label>
                <input name="password" className="form-control" id="password" onChange={this.handleChange} type="password" placeholder="Password"></input>
                <br></br>
                <button className="btn btn-primary">Login</button>
            </form>
            <p>Dont have an account?</p>
            <div>
            <span>Register <NavLink to="/register">here</NavLink></span>
            <br />
            <span className="float-right"><NavLink to="/forgot-password">forgot-password</NavLink></span>

            </div>
                
            
            </div>
        );
    }
}


