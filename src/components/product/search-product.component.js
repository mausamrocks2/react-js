import React from 'react';
import http from './../../utilities/httpclient';
import notify from '../../utilities/notify';
import { ViewProduct} from './view-product.component'; 

export const DefaultForm = {
    name: null,
    category: null,
    description: null,
    brand: null,
    price: null,
    image: null,
    color: null,
    batchNo: null,
    manuDate: null,
    expiryDate: null,
    origin: null,
    discountedItem: null,
    discountType: null,
    discountValue: null,
    weight: null,
    tags: null,

}  //default form bhitra maile yesari form banaidiye

export class SearchProduct extends React.Component
{
constructor(props)
{
    super(props);
    this.state = {                 //j jati pani app ko data cha statenai ho
        data: {                //data backendma pathaunako lagi
            ...DefaultForm
        },
        error: {                 //form form validation
            ...DefaultForm
        },
        validForm: false,  //validform false huda disable hunuparyo
        isSubmitting: false,
        isLoading: false,
        categories: [],      //yaha array rakhnai parcha kinabhane maile map gareko chu render  ma first mai
        showName: false,
        allProducts: [], 
        searchResult: [],
        
    }  

} 


componentDidMount()
 {
    http.get('/product/search', { body: {} }, true)
    .then((data) => {
        console.log('data>>>', data);
        let categories = [];
        
        data.forEach((item, i) => {

            if(categories.indexOf(item.category) === -1)
            {
                categories.push(item.category);
            } 
        
        })
        this.setState({
            categories,
            allProducts: data,
        });
    })
}
    


handleChange = (e) => {    //bind(this) gari rakhnu parena
    console.log('event>>',e.target);
    const { type, checked, name } = e.target;            //state ma data rakhne kaam garnuparyo
                                                //similar to e.target.name,value...
    let { value } = e.target;

    if(type === 'checkbox')
    {
     value = checked;
    } 
    console.log('value>>', value);
    console.log('name>>', name);


    
    if(name === 'category')
    {
        this.setState({
            showName: true
        })
    }

    let toDate = (name === 'multipleDateRange' && value === false)
    ? null
    : value

    this.setState((previousState) => ({   
        data: {
            
            ...previousState.data,             //euta change garda purano bigarnu bhayenana .data
                                                //previous state ma bhako data manai
            [name]: value,
            toDate: toDate,
        } 
    }), () => {                                 //state set bhayepachi garine codeharu callback() ma rakhincha
        
        this.validateErrors(name);              //FORM VALIDATION
    })
}
validateErrors(fieldName) {
    let error;

    //prepare logic
    switch (fieldName) {
        case 'name':
            error = this.state.data[fieldName].length ? '' : 'Name is required'
            break;
        case 'category':
            error = this.state.data[fieldName].length ? '' : 'Category is required'
            break;
        default: 
            break;

    }
    this.setState((previousState) => ({
        error: {
            ...previousState.error,
            [fieldName]: error
        }
    }), () => {
        this.validateForm();
    });
}

    validateForm() {                 //validate form is a method 
        
        var errors = Object          //errors is an array
        .values(this.state.error)
        .filter(err => err)           //if(err) cha bhane return true garya jastai ho


        console.log('errors>>', errors);     //errors chai hamro array ma huncha
       
        this.setState({
            validForm: errors.length === 0,  //errorko length 0 bhakai belama validform true hunu paryo
        })
    }

handleSubmit = (e) => {
    e.preventDefault() ; //page reload hudaina
    // console.log("submit clicked");
    this.setState({
        isSubmitting: true
    })

    http.post('/product/search', { body: this.state.data }, true)  //product.route ma add bhanne raina raicha,secured ho bhane token pathaunu parcha true..
    .then((data) => {
        if( !data.length)
        {
        notify.showSuccess("no any products matched yout searched query");
        this.props.history.push('/product/view');
    }
    else 
    {
        this.setState({
            searchResult: data
        });
    }
    })    
    .catch(err => {
        this.setState({ isSubmitting: false });  //pheri tyo clickable ta hunuparyoni, true bhaneko token pathauney ki napathauney...
        notify.handleError(err);
    });
}

//mandatory method for stateful components
render()
{
    let selectOptions = this.state.categories.map((item, i ) => (
        <option key= {i} value= {item} > {item} </option>

    ))

    let selectOptionsForName = this.state.allProducts
    .filter(item => item.category === this.state.data.category)
    .map((item, i) => (
        <option key={i} value={item.name}>{item.name}</option>
        
    ))

    //sake samma application ko logic render bhitra
    
    let button = this.state.isSubmitting
    ? <button type="submit" disabled className=
    "btn btn-info">Submitting...</button>    //submitting taba false huncha jaba backend bata err response aucha
    : <button disabled={ !this.state.validForm } type="submit" className="btn btn-primary">Submit</button>  
     //false bhako bela disabled
    //submit bhako case ma disabled banauney ho, submitting ma ta always disabled...
    

    let nameContent =  
    <div>
    <label htmlFor="name">name</label>
    <select name="name" className="form-control" onChange={this.handleChange}>
        <option selected disabled value=" ">(Select Name)</option>
        {selectOptionsForName}
    </select>
    <p>{this.state.error.name}</p>
    </div>
   

    let name = this.state.showName
    ? nameContent 
    : '';

    let toDateContent = 
    <div>
                <label htmlFor="toDate">To Date</label> 
                <input className="form-control" id="toDate" type="date" 
                 name="toDate" onChange={this.handleChange} />  

    </div>

    let toDate = this.state.data.multipleDateRange
    ? toDateContent
    : '';
     

    //show search result
    let mainContent = !this.state.searchResult.length
    ? <div>   
    <h2>Search Product</h2>
    <form className="form-group" onSubmit={this.handleSubmit} noValidate>
        {/* <label htmlFor="name">name</label>   
        <input className="form-control" id="name" type="text" placeholder="name"
         name="name" onChange={this.handleChange} />
         <p>{this.state.error.name}</p> */}

        <label htmlFor="category">category</label>   
        <select name="category" className="form-control" onChange= {this.handleChange} >
        <option defaultValue="selected" disabled value= ""> (Select Category) </option>
            
        {selectOptions}

        </select>
         <p>{this.state.error.category}</p>
         {name}

        <label htmlFor="brand">brand</label>   
        <input className="form-control" id="brand" type="text" placeholder="brand"
         name="brand" onChange={this.handleChange} />


        <label htmlFor="minPrice">Min Price</label>   
        <input className="form-control" id="minPrice" type="text" placeholder="minPrice"
         name="price" onChange={this.handleChange} />

        <label htmlFor="maxPrice">Max Price</label>   
        <input className="form-control" id="maxPrice" type="text" placeholder="maxPrice"
         name="maxPrice" onChange={this.handleChange} />

        <label htmlFor="color">color</label>   
        <input className="form-control" id="color" type="text" placeholder="color"
         name="color" onChange={this.handleChange} />

        <label htmlFor="weight">weight</label>   
        <input className="form-control" id="weight" type="text" placeholder="weight"
         name="weight" onChange={this.handleChange} />

        <label htmlFor="tags">tags</label>   
        <input className="form-control" id="tags" type="text" placeholder="tags"
         name="tags" onChange={this.handleChange} />

        <label htmlFor="fromDate">From Date</label>   
        <input className="form-control" id="fromDate" type="date"
         name="fromDate" onChange={this.handleChange} />
        
        <input type="checkbox" id="multipleDateRange" name="multipleDateRange" onChange={this.handleChange} />
        <label htmlFor="multipleDateRange">Multiple Date Range</label>
        
         {toDate}

        <br />
        {/* {button}    mathiko button load gareko */}
        { button }
         
    </form>
</div>
: <ViewProduct data={this.state.searchResult} />

    return (
         mainContent 
    )
}
}
  