import React from 'react';
import http from './../../utilities/httpclient';
import notify from './../../utilities/notify';
import { Link } from 'react-router-dom';

export class ViewProduct extends React.Component
{
    constructor()
    {
        super();
        this.state = {
            products: [],
            isLoading: false,

        }  
    }

componentDidMount()  
{  
//life cycle of component         
//render bhaisakepachi aucha, aba content pathaine render bhitra
//protected route haru dherai cha bhane default ma true and vice versa
console.log('props>>>',this.props);

if(this.props.data)
{
    this.setState({
        products: this.props.data,
    })
}
else
{
    http.get('/product', {}, true)
    .then((data) => {
     console.log('products>>',data);
        this.setState({
            products: data,

        })
    })
        .catch((err) => {
            // console.log('err>>',err)
            notify.handleError(err);
        })
    

    }
}



handleEdit = (e) => {
    e.preventDefault();
}

handleDelete = (id, i) => {
    console.log('id>>', id);
    
    http.delete(`/product/${id}`, {}, true)     //headers haru afai aucha
    .then((data) => {
        notify.showInfo("product removed successfully");
        const { products } = this.state;
        products.splice(i, 1);
        this.setState({ 
            products 
        });
        
    })
        .catch((err) => {      
            notify.handleError(err);
        
        }) 
    }


render() 
    { 

    // let imgUrl = process.env.REACT_APP_IMG_URL;

    let data = this.state.products.map((product, i) => {
        let redirectURL = `/product/edit/${product._id}`;
        // let img_url = ` ${imgUrl}${product.image}`
    
        return (
                   
            //_id nabhalo case ma index diney,coz index is always unique
            //key haru loop ma lagcha, yaha row ma lageko cha
            //loop launey bhaneko js ko map use garerai ho */
            <tr key={product._id}>   
                <td>{i + 1}</td>
                <td>{product.name}</td>
                <td>{product.category}</td>
                <td>{product.price}</td>
                <td>{product.color ? product.color : 'N/A'}</td>
                <td>
                      
                    {/* <img src={img_url} alt = "product_img.png" width="200px"></img> */}

                </td>
                <td>
                <Link to= {redirectURL}>

                    <button className="btn btn-info">   
                        edit 
                    </button>
                    </Link>

                    
                    <button className="btn btn-primary" onClick={() => this.handleDelete(product._id, i)}>
                         delete    
                    </button>
                </td>

            </tr>
        )
        
    })





    
    
    return (
        <div>
            <h2>View Products</h2>
            <table className="table table-bordered">
                <thead>
                    <tr>
                    <th>S.N</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Color</th>
                    <th>Images</th>
                    <th>Actions</th>
                    </tr>

                </thead>
                <tbody>        
                    {data}
                </tbody>
            </table>
        </div>
    )
    }
}

