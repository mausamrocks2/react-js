import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import http from '../../utilities/httpclient';
import notify from '../../utilities/notify'; 
const defaultForm = {
    email: null
}


export class ForgotPassword extends Component
{
    constructor()
    {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            validFrom: false,
            isSubmitting: false,
        }
        
    }

    

    handleSubmit = (e) => {

        e.preventDefault();
        this.setState({
            isSubmitting: true
        });

        http.post('/auth/forgot-password',{ body: this.state.data })
        .then((data)  => {
            notify.showInfo('password reset link sent to your email, please check');
            this.props.history.push('/');
        })
        .catch((err) =>  {
            this.setState({
                isSubmitting: false
            });
            notify.handleError(err);
        });

    }


    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState((previousState) => ({
                data: {
                    ...previousState.data,
                    [name]: value
                }
        }), () => {
            this.validateError(name);
        })
    }

    validateError(fieldName) {
        let errMsg;
        switch (fieldName)
        {
            case 'email':
                errMsg =  this.state.data.email 
                    ? this.state.data.email.includes('@')
                    ? ''
                    : 'Not a valid Email'
                    : 'Email is required';

            break;

        default: 
            break;
        }
    this.setState((err) => ({     //err ma previousState nai aune ho
        error: {
            ...err.error,
            [fieldName]: errMsg
        }
    }), () => {
        this.validateForm();
    })
}


validateForm()
{
    const errors = Object
        .values(this.state.error)
        .filter(val => val);

    console.log('errors>>>', errors);
    if (!errors.length) 
    {
        this.setState({
            validFrom: true
        })
    }
}




    
    

    



    render()
    {
        let btn = this.state.isSubmitting
        ? <button disabled type="submit" className="btn btn-info" >Submitting...</button>
        : <button disabled={!this.state.validFrom} type="submit" className="btn btn-primary" >Submit</button>
        

        return  (
            <>
            <h2>Forgot Password</h2>
            <p>Please enter your email</p>
            <form className="form-group" onSubmit={this.handleSubmit}>
                <label>Email</label>     
                <input className="form-control" name="email" onChange={this.handleChange} type="text" placeholder="Email"></input>             
                {/* we say controlled input */}
                <p>{this.state.error.email}</p>
                <br/>
                { btn }
                
            </form>
            <br />
            <p>
                Back to <Link to="/">login</Link>
            </p>

            </>

        )
    }
}