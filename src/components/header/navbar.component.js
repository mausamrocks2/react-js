import React from 'react';
import { Link,withRouter } from 'react-router-dom';   //router ko props ma aune valueharu chaulauna withRouter
import './navbar.component.css';


const NavBar = (props) => {

    const logout = (e) => {
        localStorage.clear();
        props.history.push('/');  //redirect to login
    }


    return(
        <nav>
            <ul className="nav-list" >
                <li className="nav-items">
                    <Link to="/">Profile</Link>
                </li>

                <li className="nav-items">
                    <Link to="/menu">Change-Password</Link>
                </li>

                <li className="nav-items">
                    {/* <Link to="/">Log-Out</Link> */}
                    <button className="btn btn-default" onClick={logout}>LOGOUT</button>

                </li>

                
            </ul>
        </nav>
    )
    }

export default withRouter(NavBar);